from channels.http import AsgiHandler
from channels.routing import ProtocolTypeRouter, URLRouter

from django.conf.urls import url

from . import views

application = ProtocolTypeRouter({
    "http": URLRouter([
        url(r'^(?P<access_code>[a-zA-Z]{6})/', URLRouter([
            url(r"status/(?P<hashcode>[a-z0-9]{64})/",
                views.StatusLongPollConsumer, name='status'),
        ])),
        url(r"", AsgiHandler),
    ]),
})
