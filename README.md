# Two Rooms and a Boom (2r1b)

Unofficial web app implementation of the board game Two Rooms and a Boom.

[Two Rooms and a Boom](http://www.tuesdayknightgames.com/tworoomsandaboom/)
was designed by by Alan Gerding and Sean McCoy and published by
[Tuesday Knight Games](http://www.tuesdayknightgames.com/).

The purpose of this app will be to support both in-person and online
games of Two Rooms and a Boom with support for text and voice chat.

This app has not started development yet. See the [todo](todo) and
[design](design) files for plans.
